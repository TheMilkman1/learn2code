while 1
    prompt = 'how many days have ya got on your pass? ';
    dayz = input(prompt)

    daysChasing = 100;

    seasonStart = datetime('9-nov-2018');
    seasonEnd = datetime('14-may-2019');
    today = datetime('today');

    % The line you want to be above
    t = seasonStart:seasonEnd;
    y = linspace(0, 100, length(t));
    plot(t,y,'--r')
    hold on

    % Where you're at
    td = seasonStart:today;
    y2 = linspace(0,dayz,length(td));
    plot(td,y2,'b')

    daysLeft = between(today, seasonEnd, 'days');
    daysNeeded = 100 - dayz;

    % Cast from duration
    daysLeft = split(daysLeft, {'days'});
    daysPerWeek = daysNeeded/(daysLeft/7);

    title(['You need ',num2str(daysNeeded),' days to hit 100, which is ', num2str(daysPerWeek), ' days a week'])
    grid on
    xlabel('Date') 
    ylabel('Days') 
    hold off
end