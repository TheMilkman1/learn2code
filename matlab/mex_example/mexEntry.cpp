#include <stdio.h>
#include <stdlib.h>
#include "mex.h"

#include "try.hpp"
#include "mexedFunctions.cpp"

// nlhs = number of arguments on the left hand side (outputs)
// nrhs = number of arguments on the right hand side (inputs)
void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double multiplier; // Input scalar
    
    double *inVector;  // Pointer to the varible used in the function
    double *outVector; // This simplifies by always using a return void function
    
    size_t nrows;
    
    // Error Handling
    if (nrhs != 2)
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:nrhs", "Two inputs required");
    if (!mxIsDouble(prhs[0]) || mxIsComplex(prhs[0]) || mxGetNumberOfElements(prhs[0]) != 1)
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notScalar", "not a scalar");
    if (!mxIsDouble(prhs[1]) || mxIsComplex(prhs[1]) || mxGetN(prhs[1]) != 1)
        mexErrMsgIdAndTxt("MyToolbox:arrayProduct:notDoubleVector", "not a column vector");
    
    // Unpack incoming data
    multiplier = mxGetScalar(prhs[0]);
    inVector = mxGetPr(prhs[1]);
    nrows = mxGetM(prhs[1]);
    
    // Set up outoing data
    plhs[0] = mxCreateDoubleMatrix(nrows, 1, mxREAL);
    outVector = mxGetPr(plhs[0]);
    scalarMultiply(multiplier, inVector, outVector, nrows);
}
