#include <iostream>

void scalarMultiply(double multiplier, double *inVector, double *outVector, size_t nrows)
{
    size_t ii;
    
    for (ii = 0; ii < nrows; ii++) {
        outVector[ii] = multiplier * inVector[ii];
    }
}
